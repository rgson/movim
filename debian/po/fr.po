# Translation of movim debconf templates to French
# Copyright (C) 2018 Debian French l10n Team <debian-l10n-french@lists.debian.org>
# This file is distributed under the same license as the movim package.
#
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: movim\n"
"Report-Msgid-Bugs-To: movim@packages.debian.org\n"
"POT-Creation-Date: 2018-12-17 00:19+0100\n"
"PO-Revision-Date: 2018-12-27 19:15+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr_FR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 2.0\n"

#. Type: string
#. Description
#: ../movim.templates:1001
msgid "Admin username:"
msgstr "Nom d'utilisateur de l'administrateur :"

#. Type: string
#. Description
#. Type: password
#. Description
#: ../movim.templates:1001 ../movim.templates:2001
msgid ""
"Movim has an administration tool to configure various settings, which is "
"protected by an admin user and password."
msgstr ""
"Pour configurer certains réglages, Movim possède un outil d'administration "
"qui est sécurisé par un administrateur avec un mot de passe."

#. Type: string
#. Description
#: ../movim.templates:1001
msgid "Please provide the username to login to the backend."
msgstr "Veuillez fournir le nom d'utilisateur pour se connecter au programme."

#. Type: password
#. Description
#: ../movim.templates:2001
msgid "Admin password:"
msgstr "Mot de passe de l'administrateur :"

#. Type: password
#. Description
#: ../movim.templates:2001
msgid "Please provide the password to login to the backend."
msgstr "Veuillez indiquer le mot de passe pour se connecter au programme."

#. Type: string
#. Description
#: ../movim.templates:3001
msgid "Public URL of Movim instance:"
msgstr "URL publique de l'instance de Movim :"

#. Type: string
#. Description
#: ../movim.templates:3001
msgid ""
"The public URL is the URL at which this instance of Movim will be reachable. "
"It is used in various places to generate endpoint URLs."
msgstr ""
"L'URL publique est l'URL à laquelle cette instance de Movim sera joignable. "
"Elle est utilisée à divers emplacements pour créer des URL de point d'accès."

#. Type: string
#. Description
#: ../movim.templates:3001
msgid ""
"Please make sure to use the right protocol (HTTP vs. HTTPS), the correct "
"public hostname and the correct base path expected by your webserver. The "
"WebSocket endpoint is expected at .../ws/ below this URL."
msgstr ""
"Veuillez vous assurer d'utiliser le bon protocole (HTTP ou HTTPS), le nom "
"d'hôte public correct et le chemin correct de la base attendu par le "
"serveur web. Le point d'accès WebSocket se trouve à cette URL avec .../ws/ "
"en aval."

#. Type: string
#. Description
#: ../movim.templates:4001
msgid "WebSocket port:"
msgstr "Port WebSocket :"

#. Type: string
#. Description
#: ../movim.templates:4001
msgid ""
"This port is used internally and normally accepts reverse-proxied "
"connections from a public-facing wbserver."
msgstr ""
"Ce port est utilisé en interne et accepte normalement des connexions "
"utilisant un mandataire inverse à partir d'un serveur web destiné au public."
